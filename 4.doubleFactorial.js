// Напишите функцию, которая вычисляет двойной факториал числа 
// Пример:
// doubleFactorial(0) ➞ 1
// doubleFactorial(2) ➞ 2
// doubleFactorial(9) ➞ 945
// doubleFactorial(14) ➞ 645120

function doubleFactorial(a) {
    if (Number.isInteger(a) === false || a < -1) {
        return 'A negative number or not an integer'
    } else {
        return result(a)
    }
}

function result(a) {
    if (a === 1 || a === 0 || a === -1) {
        return 1;
    }
    return a * result(a - 2);
}

console.log(doubleFactorial(0))
console.log(doubleFactorial(2))
console.log(doubleFactorial(9))
console.log(doubleFactorial(14))
console.log(doubleFactorial(-1))
console.log(doubleFactorial(-10))
console.log(doubleFactorial(2.1))
console.log(doubleFactorial('word'))
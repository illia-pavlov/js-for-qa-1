// Есть два массива случайной длины заполненные случайными числами. 
// Вам нужно написать функцию, которая считает сумму всех элементов обоих массивов.
// Пример:
// a = [1, 2, 3, -5, 0, 10], b = [5, -1, 7] -> 22

const sumElementsTwo = (x, y) => {
    let z = x.concat(y)
    let sum = 0
    for (let i of z) {
        sum += i
    }
    return sum
}

const a = [1, 2, 3, -5, 0, 10]
const b = [5, -1, 7]

console.log(sumElementsTwo(a, b))
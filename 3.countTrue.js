// Дан массив, в котором содержатся элементы true и false. 
// Написать функцию, которая подсчитает количество элементов true в массиве. 
// Пример:
// countTrue([true, false, false, true, false]) ➞ 2
// countTrue([false, false, false, false]) ➞ 0
// countTrue([]) ➞ 0

const countTrue = (x) => {
    const filterTrue = x.filter(element => element === true);
    return filterTrue.length
}

const a = [true, false, false, true, false]
const b = [false, false, false, false]
const c = []

console.log(countTrue(a))
console.log(countTrue(b))
console.log(countTrue(c))
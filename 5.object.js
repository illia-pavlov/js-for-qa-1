// Дано два объекта person, в них содержатся поля name и age.

// 5.1 Написать функцию compareAge, которая будет сравнивать два объекта по возрасту и возвращать шаблонную строку как результат. 
// Шаблонная строка выглядит следующим образом: <Person 1 name> is <older | younger | the same age as> <Person 2 name>

const compareAge = (a, b) => {
    if (a.age > b.age) {
        console.log(`${a.name} is older than ${b.name}`)
    } else if (a.age == b.age) {
        console.log(`${a.name} is the same age as ${b.name}`)
    } else {
        console.log(`${a.name} is younger than ${b.name}`)
    }
}

let personOne = { name: 'John', age: 22 }
let personTwo = { name: 'Mary', age: 21 }

compareAge(personOne, personTwo)

// 5.2 Создать массив из нескольких объектов person (3-5 элементов) и отсортировать его по возрасту: 
// а) по возрастанию б) по спаданию

function sortAsc(a, b) {
    if (a.age > b.age)
        return 1
    if (a.age < b.age)
        return -1
    return 0
}

function sortDesc(a, b) {
    if (a.age < b.age)
        return 1
    if (a.age > b.age)
        return -1
    return 0
}

const personAge = [
    { name: 'John', age: 45 },
    { name: 'Mary', age: 50 },
    { name: 'Sam', age: 25 },
    { name: 'Kane', age: 45 }
]

console.log(personAge.sort(sortAsc))

console.log(personAge.sort(sortDesc))
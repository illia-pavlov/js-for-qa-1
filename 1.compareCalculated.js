// Напишите код, который будет присваивать переменной "result" значение 
// суммы переменных "x" и "y" - в случае если x < y, разность "x" и "y" - в 
// случае если x > y, и их произведение в остальных случаях.
// Пример:
// x = 10, y = 20 -> result = 30
// x = 20, y = 10 -> result = 10
// x = 10, y = 10 -> result = 100

function compareCalculated(x, y) {
    let result
    if (x < y) {
        result = x + y
    } else if (x > y) {
        result = x - y
    } else {
        result = x * y
    }
    return result
}

console.log(compareCalculated(10, 20));
console.log(compareCalculated(20, 10));
console.log(compareCalculated(10, 10));